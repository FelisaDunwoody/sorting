README
======


Summary

       





This application will help you to find the best algorithm is the current case of your data. There are several methods that are developed for sorting, each method is developed for a special case.
The most popular sorting algorithms are :-
     
    * Insertion Sort
    * Merge Sort 
    * Bubble Sort 
    * Selection Sort 
    * Heap Sort 
    * Quick Sort
    * Simple Sort 

The idea sorting algorithm, that will be better in all the cases should have O(1) times for processing the data, in which worse case would take O(n·lg(n)) key comparisons and O(n) for swaps. Learn more about sorting algorithms and their use by using this helpful friendly App.

Felisa Dunwoody -- Blog Editor At -- <http://www.psychic-abigail.com/>